$(function () {
	'use strict';

	// Efectito menu
	$('#sacar_menu').click(function () {
		$('#sacar_menu').removeClass('sacar');
		$('#sacar_menu').addClass('meter');
		$('.menusito nav').removeClass('meter');
		$('.menusito nav').addClass('sacar');
		$('.container, .footer, .images_header').removeClass('meter');
		$('.container, .footer, .images_header').addClass('sacar');
	});

	$('#meter_menu').click(function () {
		$('#sacar_menu').removeClass('meter');
		$('#sacar_menu').addClass('sacar');
		$('.menusito nav').removeClass('sacar');
		$('.menusito nav').addClass('meter');
		$('.container, .footer, .images_header').removeClass('sacar');
		$('.container, .footer, .images_header').addClass('meter');
	});

});
